package brazojavamvc.modelos;

public class BrazoModel {

    public BrazoModel(int anguloBase, int anguloMedio, int anguloPinza) {
        this.anguloBase = anguloBase;
        this.anguloMedio = anguloMedio;
        this.anguloPinza = anguloPinza;
    }

    @Override
    public String toString() {
        return "BrazoModel{" +
                "anguloBase=" + anguloBase +
                ", anguloMedio=" + anguloMedio +
                ", anguloPinza=" + anguloPinza +
                '}';
    }

    public int getAnguloBase() {
        return anguloBase;
    }

    public void setAnguloBase(int anguloBase) {
        this.anguloBase = anguloBase;
    }

    public int getAnguloMedio() {
        return anguloMedio;
    }

    public void setAnguloMedio(int anguloMedio) {
        this.anguloMedio = anguloMedio;
    }

    public int getAnguloPinza() {
        return anguloPinza;
    }

    public void setAnguloPinza(int anguloPinza) {
        this.anguloPinza = anguloPinza;
    }

    private int anguloBase, anguloMedio, anguloPinza;


}
