package brazojavamvc.vistas;

import brazojavamvc.modelos.BrazoModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BrazoVista {
    private JButton guardarPosiciónButton;
    private JButton cargarPosiciónButton;
    private JSlider pinzaSlider;
    private JSlider baseSlider;
    private JSlider medioSlider;
    private JPanel mainPannel;
    private JPanel brazoPanel;

    public BrazoVista() {

        guardarPosiciónButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                BrazoModel modelo = new BrazoModel(baseSlider.getValue(), medioSlider.getValue(), pinzaSlider.getValue());

                JOptionPane.showMessageDialog(null, "se esta guardando el modelo "+ modelo);

                //TODO Implementar la logica del guardado hay un ejemplo en el archivo proyecto carro

            }
        });


        pinzaSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider)changeEvent.getSource();
                if(!source.getValueIsAdjusting())
                {
                    //TODO modificar el dibujo del brazo
                    JOptionPane.showMessageDialog(null, "La pinza esta a " + pinzaSlider.getValue() + " Grados ");
                }
            }
        });

        medioSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider)changeEvent.getSource();
                if(!source.getValueIsAdjusting())
                {
                    //TODO modificar el dibujo del brazo
                    JOptionPane.showMessageDialog(null, "El medio esta a " + medioSlider.getValue() + " Grados ");
                }
            }
        });

        baseSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JSlider source = (JSlider)changeEvent.getSource();
                if(!source.getValueIsAdjusting())
                {
                    //TODO modificar el dibujo del brazo
                    JOptionPane.showMessageDialog(null, "la Base esta a " + baseSlider.getValue() + " Grados ");
                }
            }
        });

        cargarPosiciónButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                //TODO la logica para cargar un estado del brazo
                JOptionPane.showMessageDialog(null, "Pendiente por implementar");
            }
        });
    }

    public static void main(String[] args) {
        JFrame ventana = new JFrame("Brazo mecanico");

        ventana.setContentPane(new BrazoVista().mainPannel);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.pack();
        ventana.setVisible(true);
    }
}
